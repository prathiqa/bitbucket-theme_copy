# bitbucket-theme

A repository for storing multiple implementations of the Bitbucket syntax highlighting theme.

[Preview Textmate Theme](https://tmtheme-editor.herokuapp.com/#!/editor/url/https://bitbucket.org/bitbucket/bitbucket-theme/raw/master/bitbucket.tmTheme)

[Preview Dark Textmate Theme](https://tmtheme-editor.herokuapp.com/#!/editor/url/https://bitbucket.org/bitbucket/bitbucket-theme/raw/master/bitbucket-dark.tmTheme)

#This is a sample update to raise PR

